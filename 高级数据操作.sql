高级数据操作
    新增数据:
        多数据插入

            只要写一次insert指令
            可以直接插入多条记录
                基本语法:
                    insert into 表名 [字段列表] values (值列表);
                    新的方式：在值列表之后，能够跟上多个值列表,
                    insert into student values('tom'),('mary');
        主键冲突
            主键冲突：在有的表中，使用的是业务主键（字段有业务含义），但是往往在
            进行数据插入的时候。不确定数据表中是否已经存在对应的主键。

            create table my_student_k(
                stu_id varchar(10) primary key comment '主键：学生id',
                stu_name varchar(10) not null comment '学生姓名'
            )charset utf8;
            
            insert into my_student_k values
            ('stu0001','tom'),
            ('stu0002','mary'),
            ('stu0003','sili'),
            ('stu0004','pepinot'),
            ('stu0005','peng');

            insert into my_student_k values ('stu004','good game');
            -- error

            主键冲突的解决方案：
                主键冲突更新：
                    类似插入数据语法：
                    如果插入过程中主键冲突,那么采用更新方法
                    insert into [字段列表] values [值列表] on duplicate key update 字段 = 新值

                    冲突更新:
                        insert into my_student_k values ('stu004','good game') on duplicate key update name = 'Good game';

                    主键冲突替换：
                        replace into [字段列表] values [值列表];
        蠕虫复制
            蠕虫复制，一分为二，成倍的增加
                从已有的数据中获取数据，并且将获取到的数据直接插入到数据表中。
                基本语法：
                    insert into table 表名 [字段列表] select [字段列表] from 其他的数据表
                    
                创建实例：
                    create table my_simple(
                        name char(1) not null comment '姓名'

                    )charset utf8;

                插入数据
                insert into my_simple values('a'),('b'),('c'),('d');

                蠕虫复制:
                    insert into my_simple(name) select name from my_simple;

                蠕虫复制的意义：
                    1.蠕虫复制，复制重复数据，可以在短期内快速增加表的数据流
                      从而可以测试表的压力，可以通过大量数据来测试表的效率（索引）
                    2.蠕虫复制虽好，但是要注意主键冲突。
                    3.通过两条sql指令，可以把整个表的数据和结构给复制到新的表里
        更新数据：
            在更新数据的时候一定要注意，一般情况下的表数据更新都要跟随条件更新
                update 表名称 set 字段名称 = 新值 where 判断条件，
            如果没有设置判断条件的话，最后的结果将是全表更新。但是可以使用limit
            来限制更新的数量
                update 表名称 set 字段名称 = 新值 [where 判断条件]limit 限制数量;

            改变四个a变成e
            select * from my_simple;
            update my_simple set name ='e' where name = 'a' limit 4;
        删除数据操作:
            1.删除数据的时候尽量不要全部删除，要使用where对范围进行判定
            2.删除数据的时候使用limit来限制要删除的具体数量

            delete 删除数据，无法重置 auto_increment（自增长）

            mysql有个能够重置表选项自增长的语法：
            Truncate 表名;====> drop-->create
        查询数据:
            完整的查询指令
            select select选项 字段列表 from 数据源
             where 条件 group by 分组 having 条件 order by 排序 limit 限制;

            1.select 选项:系统如何对待查询得到的结果
            ALL:保存所有的记录
            Distinct：去重，去除重复的记录，只保留一条(所有的字段都相同)

            select all * from my_simple;
            select distinct * from my_simple;

            2.字段列表：有的时候需要从多张表中获取数据，可能存在不同表中有同名的数据
                需要将同名的字段命名成不同名的，使用别名:alias

            基本语法：字段名 [as] 别名
            字段取别名
            select distinct name as name1,name name2 from my_simple;

            From 数据源
                from是为前面的查询提供数据,数据源只要是一个符合二维表结构的数据即可
                单表数据：
                    from 表名
                多表数据:
                    从多张表获取数据。
                    基本语法：from 表1，表2...
                    select * from my_int,my_simple;
                    两张表记录数相乘，字段数拼接
                    本质，从第一张表去除一条记录，去拼凑第二张表的所有记录,
                    保留所有的结果。得到的结果在数学上有个专业的说法，笛卡尔积
                    这个结果除了给数据库造成压力，没有任何好处

                    尽量避免笛卡尔积
                动态数据：
                    from 后面跟的数据不是一个实体表，而是从表中查询出来的二维结果表
                    子查询
                    from （select * 字段列表 from 表名） as 别名;
                    给表作别名
            where 子句:
                用来从数据表获取数据的时候进行条件筛选
                数据获取原理，判断是否符合条件，如果符合就保存下来，如果不符合就直接舍弃
                不放到内存中
                where通过运算符进行结果比较来判断数据
            group by子句：
                分组的含义，根据指定的字段，将数据进行分组，分组的目的是为了统计
            基本语法：
                    group by 字段名称;
                select * from my_student group by class_id;
                根据班级进行分组
                group by是为了分组后进行数据统计的，如果只是想看数据显示，那么groupby没有什么意义
                groupby 将数据按照指定的字段分组后 只会保留每一组的第一条记录

                聚合函数
                count():统计每一组的数量，如果统计目标是字段，那么不统计为null字段
                如果为*号，则统计记录
                avg():统计每一个字段的平均数
                sum():求和 
                max(),min()
                
                example:
                    select class_id,count(*),max(stu_age),min(stu_height),avg(stu_age) from my_class group by class_id;

                    group_concat_连接

                    select class_id,group_concat(stu_name),count(*),max(stu_age),min(stu_height),avg(stu_age) from my_class group by class_id;

            多分组：
                将数据按照某个字段进行分组之后，对已经分组的数据进行再次分组
                基本语法:
                    group by 字段一,字段二;
                    先按照字段一进行排序，之后按照字段二进行排序，以此类推
            分组排序:
                mysql中分组默认有排序的功能，按照字段排序，默认是升序
                基本语法
                    group by 字段 [asc/desc],字段 [asc/desc] 
                分组完后可以不用再进行order by
            回溯统计:
                当数据进行多分组之后，往上统计的过程中，需要层层上报,将这种层层上报
                的统计过程称为回溯统计：每一次分组向上统计的过程都会产生一次性的统计数据
                而且当前数据对应的分组字段为null
                group by [asc/desc] with rollup;
                select class_id,gender,count(*) from my_student group by class_id with rollup;
        Having 子句：
            having的本质和where一样，是用来进行数据条件的筛选。
            Having：
                是在group by子句之后：可以针对分组数据进行数据统计筛选，但是where不行
                查询班级人数大于等于4个以上的班级
                select class_id,count(*) as num from my_student group by class_id;
                where不能使用聚合函数
                having可以使用聚合函数
                聚合函数用在group by分组的时候
                只能用having来解决问题
                使用聚合函数或者字段别名
                    select class_id,count(*) as number from my_student group by class_id having number >= 4;
                    select class_id,count(*) as number from my_student group by class_id having count(*) >= 4;
                
                having在group by之后，group by 是在 where 之后 where 的时候
                表示，将数据从磁盘拿到内存，where 之后的所有操作都是内存操作
        Order by 子句:
            order by排序:根据校对规则对数据进行排序
            order by 字段 [asc/desc];
            select * from my_student order by stu_height asc;

            order by 也可以和 group by 一样 进行多字段排序
            order by 字段一 [asc/desc],字段二[asc/desc];
            按照班级和身高进行排序
            select * from my_student order by class_id desc,stu_height asc;
        limit 子句：
            主要是用来限制记录数的获取
            记录数的限制：
            纯粹的限制获取的数量，从第一条到指定的数量
            limit 数量;
            select * from  my_student limit 2;

            分页：
                利用limit来限制获取指定的区间的数据：
                limit offset,length; //offset 偏移量，
                从哪开始，length就是具体的获取多少条记录
                select * from  my_student limit 2,2;
                    