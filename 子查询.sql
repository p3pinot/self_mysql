子查询：
    sub query
    在一条select语句中，嵌入另外一条select语句，
    那么被嵌入的select语句称之为子查询语句。

主查询：
    主要的查询对象,第一条select语句，确定了用户所要获取的
    数据目标（数据源）
    以及要具体得到的字段信息
主查询和子查询的关系：
    1.子查询嵌入到主查询中
    2.子查询是辅助主查询的，要么作为条件，要么作为数据源。
    3.子查询可以独立存在，子查询是完整的select语句
子查询分类：
    1.按照功能分:
        标量子查询 子查询返回的结果是一个数据 一行一列
        列子查询 返回的结果是一列，一列多行
        行子查询 返回的结果是一行，一行多列
        表子查询 返回的结果是多行多列
        Exists 子查询 返回的结果是0/1
    2.按照位置分：
        where 子查询：子查询出现的位置在where条件中
        From 子查询: 子查询出现在from语句中,作为数据源
    1.标量子查询：
        子查询返回的结果是一个数据 一行一列
        语法：
        基本语法：  
            select * from 数据源 where 条件判断 = / <> (select 字段名 from 数据源 where 条件判断)
        //子查询查到的结果只有一个值

        example:
        知道一个学生的名字，想知道他在那个班级(班级名字)
        1.通过学生表获取该学生所在班级id
        2.通过班级id获取班级名字

        --标量子查询：
            select * from my_class where class_id = (select class_id from my_student where stu_name = 'pepinot');
            需求对应主查询，条件对应子查询

    2.列子查询：
        子查询返回的结果是一列数据，一列多行的数据
        语法：
            基本语法
            主查询 where 条件 in (列子查询)；
        example:
        想获取已经有学生在班的所有班级名字
        1.找出学生表中所有的班级ID
        2.找出班级表中对应的名字
        select name from my_class where class_id in (select class_id from my_student);
    3.行子查询:
        子查询返回的结果是一行数据，一行多列的数据
        行元素：
            行元素对应的是多个字段，多个字段合起来作为一个元素参与运算，
            将其称之为行元素。
        语法：
        基本语法：  
            主查询 where 条件 [构造一个行元素(把几个字段放在一起)] = 对应的一个子查询(行子查询)；

        example：
            获取班级上年龄最大，且身高最高的学生
            1.求出班级年龄最大的值
            2.求出班级身高最高的值
            3.求出对应的学生
            select * from my_student where (stu_age,stu_height) = (select max(stu_age),max(stu_height) from my_student);
    表子查询：
        子查询返回的是多行多列的数据
        表子查询需要不需要产生行元素，

        行子查询是用where条件进行判断，属于where子查询
        表子查询适用于from数据源，属于from子查询

        select 字段列表 from (子查询) group by order by having limit
        example:
        获取每个班上最高身高的学生
        1.将每个班最高的学生排在最前面，order by
        2.再针对结果进行group by：保留每组第一个
        select * from (select * from my_student order by stu_height desc) as ms group by class_id;

    exist子查询：
        概念：
            exists 子查询，返回结果只有0和1，1代表成立，0代表不成立
        语法：
            基本语法 where exists (查询语句);
            如果结果存在，返回1，如果不存在结果返回0
            where 1:永远为真
        example:
        求出有学生在的所有班级：
            select * from my_class as c where exists(select stu_id from my_student as s where c.class_id = s.class_id);
    关键字子查询：
        黑马程序员phpp2——p52
        in
        any
        some
        all
