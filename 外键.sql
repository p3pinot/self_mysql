外键 -- 概念
    外键表示了两个关系之间的相互联系
    如果公共关键字在一个关系中是主关键字
    那么这个公共关键字被称作另一个关系的外键。
    以
    
    另一个关系的外键作为主关键字的表被称为主表
    ，具有此外键的表被称为主表的从表。

    外键又称作为外关键字。

外键：
    foreign key:
    一张表中有一个字段，保存的值指向另一张表的主键

    a表中有一个字段，指向了b表的主键

    b:主表 
    a:从表

外键的基本操作:
    增加外键：
        1.在增加，创建表的时候，增加外键（类似主键
        2.在创建表后来增加外键
        基本语法：  
        在字段后增加一条语句
        1.[constraint `外键名字`(反引号)] foreign key(外键字段) references 主表（主键）;

        2.alter table 从表 add [constraint`外键名`] foreign key(外键字段) references 主表（主键）;

        --增加外键
        --创建外键表
        create table my_foreign(
            id int primary key auto_increment,
            name varchar(10) not null,
            --关联班级表my_class
            class_id int,       --作为外键字段
            --增加外键
            constraint foreign key(class_id) references my_class(class_id);

            foreign key(class_id) references my_class(class_id) 
        )charset utf8;


        MUL:多索引,外键本身是一个索引，外键还要求，外键字段本身也是一种普通索引
        show create table my_foreign;

        修改my_student表，将class_id 设为外键字段
        alter table my_student add constraint `student_class_ibfk_1` foreign key(class_id) references my_class(class_id);
        --有报错的可能，最新版可能会报错

        --修改、删除外键
            外键不允许修改，只能先删除后增加
            基本语法：  
                alter table 从表 drop foreign key 外键名字(我们增加的/系统帮我们增加的);

                alter table my_student drop foreign key `student_class_ibfk_1`;

                外键创建 会自动增加一个索引
                外键删除 只会删除外键自身

                外键基本要求：
                    1、外键字段需要保证与关联的主键类型完全一致;
                    2、要求基本的属性也要相同
                    3、如果是在表后增加外键，对数据有一定的要求(从表数据与主表的关联关系)
                    4、外键只能使用innodb进行存储
    外键约束:
        约束的基本概念：
            通过建立外键关系之后，对主表和从表都会有一定的数据约束效率

            1、当一个外键产生时，意味着外键所在的表（从表，会受制于主表数据的存在
            从而导致数据不能够进行某些不符合规范的操作（不能插入主表不存在的数据）

            2、如果一张表被其他表外键引入，则该表的数据操作不能随意，必须保证
            从表数据的有效性，不能随便删除一个被从表引入的数据

            --向从表插入数据 
            insert into my_foreign values(null,'小明',1);  --seccess!
            insert into my_foreign values(null,'xiaoming',4) --false!
            因为主表中没有4班,所以数据会因为外键约束的原因插入失败
            主表没有该记录

            可以在创建外键的时候，可以对外键约束进行选择性的操作    
            基本语法：  
                add foreign key(外键子弹) references 主表(主键) on 约束模式

                约束模式由三:
                    1.district:严格模式，默认 不允许操作
                    2.cascade：级联模式（一起操作）主表变化，从表数据一起变化
                    3.set null : 置空模式  主表变化（删除）从表对应记录设置为空,前提是从表字段允许为空

                外键约束的主要约束对象是主表操作，从表就是不能插入主表不存在的数据。

                在进行约束使用的时候，需要制定操作，update、delete
                常用的约束模式 on update cascade,on delete set null
                                更新级联，删除置空
                
                example:
                alter table my_student add foreign key(class_id)
                references my_class(class_id)
                 --约束开始--设置约束
                 on update cascade,
                 on delete set null;    

                更新主表
                    update my_class set class_id = 4 where class_id=2;
                    主表中原来为2班的数据变换为原来是4班是数据

                修改后的学生表也会因为外键约束的原因发生变化
                删除主表上的数据
                delete from my_class where class_id =4;

                约束的作用，保证数据的完整性，主表和从表数据要一致

                外键有非常强大的数据约束作用，可能会导致数据在后台变化的不可控
                导致程序在设计开发逻辑的时候，就没有办法很好的把握数据（业务）
                所以外键一般很少使用
                所以其他存储引擎不支持外键

                用于保证数据的关联性
