交叉连接：
    把两个表的数据与另外一张表彼此交叉
    原理:
    从第一张表依次取出每一条记录
    取出每一条记录后，与另一张表的全部记录进行挨个匹配
    没有任何的匹配条件，所有的结果都会进行保留
    记录数 = 第一张表字段数 * 第二表的记录数
    
        基本语法：
    表一 cross join 表2;
    select * from my_student cross join my_int;

    没有实际的意义
    内连接，外连接，交叉连接
    效果和from 表一，表二；
内连接：
    inner join
    从一张表里面取出所有的记录去另一张表匹配，利用匹配条件
    进行匹配，成功了则保留，失败了则放弃
    原理：
        从第一张表中取出一条记录，去另一张表中进行匹配
        利用匹配条件进行匹配
        匹配成功 ，保留，继续向下匹配
        匹配失败，向下继续如果全表都匹配失败
        结束
    基本语法：
        表一 [inner] join 表2 on 匹配条件;
        
        如果内连接没有条件，就是交叉连接

        select * from my_student inner join my_class;
        -可以使用，没有匹配条件-结果是笛卡尔积
        select * from my_student inner join my_class on class_id = id; 
        对应班级的学生取出

    3!表的设计通常容易产生同名字段，尤其是id，所以为了避免因为重名而出现错误
    通常使用
    表名.字段名 
    从而确保字段的唯一性
    select * from my_student inner join my_class on my_student.class_id = my_class.id; 
    4!如果条件中使用到的对应的表名比较长，可以通过表别名来进行简化
    select * from my_student as ms inner join my_class as mc on ms.class_id = mc.id;
    5!内容匹配到的时候，匹配的结果才会保存
    6!内连接因为不强制使用匹配条件on因此可以在数据匹配完成之后，使用where
    条件来限制，效果和on是一样的，但是推荐使用on
    select * from my_student as ms inner join my_class as mc where ms.class_id = mc.id;

    应用场景:
        对数据由精确要求的地方使用，必须保证两种表中都能进行数据匹配。

外连接：outer join
        按照某一张表作为主表(表中所有记录在最后都会保留)
        去连接另外一张表,从而得到目标数据.
    外连接分为两种:
        1.左外连接 left join
        2.右外连接 right join
    1.左连接：左表是主表
    2.右连接：右表是主表
    原理：
        1.确定连接的主表
        2.拿主表的每一条记录去匹配另外一张表（从表）中的每一条记录
        3.如果满足匹配条件：保留，不满足即不保留
        4.如果主表记录在从表中都没有匹配成功，那么也要保留记录，
        从表对应的字段值都为NULL
    语法：
        基本语法:
            左连接: 主表 left join 从表 on 连接条件
            右连接: 从表 right join 主表 on 连接条件

        select * from my_student as ms left join my_class as mc on ms.class_id = mc.id;
    主表一定会保存，从表没有数据对应的时候，将保留字段，内容变成NULL
    特点：
        1.外连接主表数据记录一定会保存，连接之后不会出现记录数少于主表(在内连接可能会有这种效果)
        2.左连接和右链接其实可以互相转换，主从表位置互换即可(但是数据对应的位置将会改变)

    应用：
        非常常用的一种获取数据的方式，作为数据获取对应主表以及其他数据(关联)
using 关键字:
    在连接查询中用来代替对应的on这个关键字，用于条件匹配
    原理： 
    1.在连接查询时，使用on的地方使用using代替
    2.使用using后，使用using的前提是对应两张表的字段是同名的（类似自然连接中的自然匹配）
    3.如果使用using关键字，那么对应的同名字段，最终只会保留一个
    语法：
    表一 [inner left right]join 表2 （同名字段列表）;//连接字段

    select * from my_student left join my_class using(class_id);