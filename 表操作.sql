
--2.先要选择数据库
    use database2;
--创建数据表
    -- 创建普通的表
    create table 表的名称(
        字段名 字段类型【字段属性】,...)
        [表选项];
    name varchar(10)
    --10个字符，不能超过
result:
    mysql> use database2;
    Database changed
    mysql> create table class(
        -> name varchar(10));
    Query OK, 0 rows affected (0.03 sec)

    --可以使用数据库可视化工具进行建表

    --两种方式可以吧表挂到指定的数据库上
    -- 1.在数据表名字前加上数据库名字，期间使用点号连接
    create table database2.class2(
        name varchar(10)
    );

--表选项,与数据库选项类似
    Engine:存储引擎 mysql具体存储数据的方式
        innodb默认存储方式
    charset:字符集 只对自己表有效
    Collate：校对集 校对集也是只对自己表有效
    
--使用表选项 
    show databases;
    create table student(
        name varchar(10)
    )engine [=] innodb/myisam charset utf-8


    example:
    create table student(
        name varchar(10)
    )charset utf8;

--复制已有表结构
    
    从已经存在的表复制一份，只复制结构，不能复制数据
    语法
    create table 新表名 like 现已存在的表名;
    只要使用数据库.表的名称即可复制另一个数据库的已有表结构


--多维度显示表
    每当一张数据表构建，那么就会在对应的数据库下创建一些文件（与存储引擎有关系）

    --显示数据表
    显示所有表
    show tables;
    匹配显示表
    show table like '匹配模式'

    example:
    show tables like 's%';
    显示表结构:
        本质含义:
        显示表中所包含的字段信息（名字，类型，属性等信息):
            describe class;
            desc teacher;
            show columns from student;
        Field = 字段名字
        Type = 字段类型【字段属性】
        Null = 我们的值是否允许为空
        Key = 索引
    --查看表创建语句
        show create table student;

    ;和\g意思相同
    \G字段在左侧竖着，数据在右侧横着
        相当于将表旋转90度
        （一般用于复杂的情况使用，多维度的显示表）

--设置表属性
    --设置表选项
        Engine,charset,Collate.

    example:将字符集改掉
    alter table 表名称 表选项[=]值;

    ：注意，如果数据库已经确定了，里面有很多数据，不要轻易修改表选项
    (可能会有意想不到的结果)

    result:
        mysql> show tables;
    +---------------------+
    | Tables_in_database2 |
    +---------------------+
    | class               |
    | class2              |
    | student             |
    +---------------------+
    3 rows in set (0.00 sec)

    mysql> alter table class charset gbk;
    Query OK, 0 rows affected (0.03 sec)
    Records: 0  Duplicates: 0  Warnings: 0

    mysql> show create table class\G;
    *************************** 1. row ***************************
        Table: class
    Create Table: CREATE TABLE `class` (
    `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=gbk
    1 row in set (0.00 sec)

    ERROR:
    No query specified

--修改表的结构
    数据库中数据表名字通常有前缀，取数据库的前两个字母加上下划线
    1.修改标的名字
        example：   rename table 表名 to 新表名;
    2.修改表选项
        example:    alter table 表名称 表选项[=]值;
    3.修改字段
        example:    alter table 表名 change 旧字段名 新字段名 字段类型 [列属性] [新位置];
    给学生表增加age字段
        alter table 表名 add [column] 字段名称 类型;
        point:默认加到表的最后面
    4.字段位置
        字段想要在表中存放的位置：First：在某某之前，最前面，放在第一个字段
                                After:放在某个具体的字段之后，需要带上字段名名称
        example: 增加字段，并且放到第一个字段之前
            alter table 表名 add 字段名称 字段类型 字段位置标志词;
    5.修改字段名称：
        alter table 表的名称 change 旧字段名 新字段名 字段类型【字段属性】
        ！！修改字段名称后，需要指定字段类型,否则会报错
        example：
            alter table student change age 年级;
    6.修改字段类型和属性：
        modify
            alter table 表名称 modify 字段名称 新类型 [新属性] [新位置];
    mysql> alter table student modify name varchar(20);
    Query OK, 0 rows affected (0.01 sec)
    Records: 0  Duplicates: 0  Warnings: 0
        
    7.删除字段:
    alter table 表的名称 drop 字段名字;

    8.删除表结构:
    drop table 表的名称[,表名2...];
    可以同时删除多个表
    example:
        drop table class;