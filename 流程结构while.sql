流程结构while:
    循环体都是需要在大型代码块中使用
    基本语法：  
        while 条件 do
            循环体(要循环执行的代码，指令)
        end while；
    
    结构标识符：
        位某些特定的结构进行命名，然后的是在某些地方使用名字
        
        基本语法：
            名字：while 条件 do
                循环体
            end while[标志名字]；

        标识符的存在主要是为了循环体中使用循环控制
        在mysql中没有continue 和 break
        有自己的关键字替代
        iterate：迭代 -》 以下的代码不执行，重新执行循环
        leave： 离开 -》 停止整个循环

        名字：while 条件 do
                if 条件判断 then    
                    循环控制语句：iterate/leave 标志名字；
                end if;
                循环体
            end while[标志名字]；
