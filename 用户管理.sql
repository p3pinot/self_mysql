用户权限管理：
    在不同的项目中，给不同的用户，开发者，不同的操作权限，
    为了保证数据库数据的安全。

    通常一个用户的密码不会长期不变，所以需要经常性的变更数据库用户密码
    确保用户本身安全（mysql客户端用户）都会用到用户权限管理

    mysql需要用客户端进行连接认证才能进行服务器操作，需要用户信息
    mysql中所有的用户信息都在mysql数据库下的user表中

    默认的在安装mysql的时候，如果不选择创建匿名用户，那么意味着所有的用户只有一个
    root超级用户

    在mysql中对应的用户管理中，是由对应的host和user组成的主键来区分用户
    user：代表用户的用户名
    host：代表本质是允许访问的客户端(ip或者主机地址) host如果是* 
    则所有的人都可以通过ip和客户端进行访问

    创建用户：
        1.root在user表中插入记录
        2.专门创建用户的sql指令
            基本语法：
                create user 用户名 identified by 明文密码;
                用户：用户名@主机地址
                主机地址：'%'（相当于任何地方都能对数据库进行访问）
            example：
                create user 'pepinot'@'%' identified by '123456';
                简化创建用户信息
                create user 'user';
                不限定客户端ip，没有密码，匿名用户，危险！数据库可能被访问

            用户是否可以进行访问:
            mysql -u用户名 -p
    
    删除用户：
        mysql 中user 是带着host本身的（具有唯一性
        基本语法：
            drop user 用户名@host;
            drop user user@'%';
    修改用户密码：
        mysql中提供了多种修改密码的方式，一般使用系统函数
        password 用于对密码进行加密处理
        1.使用专门修改密码的指令
        set password for 用户名 = password('新的密码（同样是明文密码）')
        2.使用update修改表
        update mysql.user set password = password('pasword') where user = 'username' and host = 'host';
        
