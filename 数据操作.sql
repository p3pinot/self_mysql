--插入操作
    本质含义，将数据以sql的形式存储到指定的数据表中的字段里面
    基本语法:
        example:
            insert into 表名称[(字段列表)] values (对应字段列表)

                mysql> insert into my_teacher (name,age) values('Jack',30);
                Query OK, 1 row affected (0.00 sec)

            --ps:
                后面的values对应的值列表只要和前面的字段列表类型相对应即可
                并不需要与表结构完全一致
            insert into 表的名称 values (对应表结构)
                mysql> insert into my_teacher value(
                    -> 'Mary',30);
                Query OK, 1 row affected (0.01 sec)
--查询操作
-- 通配符 * 所有字段
-- 
    1.查询表中所有的数据
        select * from 表名;
    2.查询表中部分字段:
        select 字段列表 from 表名;
    3.简单条件查询数据
    select 字段列表或者* from 表名 where 字段名称 = 值
        example:
            select name from my_teacher where age>=0; 
    
--删除操作
    基本语法:
    delete from 表名 [where 条件]
    如果没有中括号内的内容，将会默认把整个数据全部删除
    example:
        删除年龄为30岁的老师
        delete from my_teacher where age = 30;
--更新操作
    将数据进行修改通常是修改部分字段数据
    update 表的名称 set 字段名称=新值 [where 筛选条件];
