数据备份与还原：
    整库数据备份也叫作sql数据备份：
    备份的结果都是sql指令，叫做sql的一个备份

    在mysql中提供了个专门用于备份sql的客户端
    mysqldump.exe

    应用场景：
    sql备份是mysql里面非常常见的备份和还原方式
    sql备份备份数据，备份对应的sql指令(数据和表结构)

    即便是数据库遭到毁灭性的破坏，利用sql备份依然可以实现
    数据还原

    sql备份因为需要备份结构，所以产生的备份文件特别大，因此不适合特大型数据备份
    也不适合数据变化频繁型的数据库备份

    ·应用方案

    sql备份:
        用到的是专门的备份客户端，因此这个时候还没有和数据库服务器进行连接
        基本语法和mysql。exe非常相似

        mysqldump -hPup (主机端口用户密码) 数据库名字 [表一 [表二...]] > 备份文件地址

        备份有三种形式：整库备份（只要数据库名字
                       单表备份（数据库后跟多张表
                        多表备份（数据库后跟多张表
        example:
            --利用mysqldump进行sql备份（整库）
            mysqldump -hlocalhost -P3306 -uroot -ppblsz13509625 database2 > e:/php/database2_back.sql

            --多表备份
            mysqldump -uroot -p database2 my_student my_int > e:/php/student_int.sql


        查看备份的成果
    sql数据还原：
        1.利用mysql,在没登录之前可以用这个客户端进行数据还原
        mysql -hPup 数据库 < sql文件
        必须制定数据库，数据库必须要有
        2.在sql中，提供了一种导入sql指令的方式：
            source sql文件位置
            必须先进入到对应的数据库，否则不知道该怎么进行还原
        3.人为操作：
            打开备份文件，复制所有sql指令,到mysql客户端里面粘贴执行

        example：
        mysql -hlocalhost -P3306 -uroot -p database2 < e:/php/database2_back.sql

