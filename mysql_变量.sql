变量

mysql本质是一种编程语言，需要变量来进行变量储存、
mysql中很多属性控制都是通过mysql中固有的变量来实现的

系统变量
系统内部指定的变量，系统变量针对所有的用户，mysql客户端有效
查看系统中苏搜友的变量
show variables [like 'pattern']

example:
    show variables like 'autocommit'

    可以使用select来查询变量的数据值（系统变量）
    select@@变量名称
    --查看系统变量
    select@@autocommit

    --修改系统变量
    局部修改(会话级别):只针对自己当前客户端当次连接有效
    set 变量名称 = 新值;
    全局修改：针对所有的客户端 “所有时刻”都有效。
    set global 变量名 = 值;
    set @@global.变量名 = 值;

    example:
        会话修改:
            会话修改系统变量:
            set autocommit = 0;
            修改只对自己有效，别的用户没有修改


            全局修改系统变量
            --设置全局修改后，系统变量没有改变(旧客户端)
            set global autocommit = 0;
            set @@auto_increment_increment = 2;
            步长修改为2
            --全局修改只对新客户端生效

            ps:
            如果想要本次连接对应的变量修改有效，那么不能使用全局修改，只能使用会话级别修改
            (set 变量名 = 值)
会话变量(用户变量)         
            用户定义的变量，和客户端绑定，只对当前用户，
            当前客户端有效
        define：
            set @变量名=值;
            set @变量名 := 值;
        专门用于存储数据，允许将数据从表中取出储存到变量中，
        查询到的数据只能是一行数据，一个变量对应一个字段值
        mysql没有数组类型

        赋值并且查看赋值过程
        select @name = stu_name,@age = stu_age from my_student limit 1;
        错误！ --》 修改为 := 才可以
        select @name := stu_name,@age := stu_age from my_student limit 1;
        只赋值，不查看赋值过程
        select @name :=stu_name,@age := stu_age from my_student order by stu_height desc limit 1 into @name,@age;

        查看自定义变量:
            select @变量名称
            select @name
局部变量：
    使用declare关键字声明
    declare语句出现的位置一定在begin和end之间
    （在大型语句中使用，函数，储存过程，触发器）
    声明语法 declare 变量名 数据类型 [属性]
