-- 唯一键
    unique key 用来保证对应字段中数据唯一的.
    
    主键也可以保证字段唯一性，但是
    一张表只能有一个主键，
    1.唯一键在一个表中可以有多个
    2.允许字段数据为NULL
        NULL可以为多个
    

    创建唯一键
    创建唯一键和创建主键非常类似

        在增加表的时候增加唯一键标识符
            unique
        1.直接字段之后增加唯一键的标识符
        2.在所有字段之后使用unique key('字段列表')
        3.在创建完表后也可以增加唯一键
            alter table 表名 add unique key('字段列表');\

        create table my_uniq1(
            id int primary key auto_increment,
            username varchar(10) unique
        )charset utf8;

        create table my_uniq2(
            id int primary key auto_increment,
            username varchar(10),
            unique key(username)
        )charset utf8;

        create table my_uniq3(
            id int primary key auto_increment,
            username varchar(10)
        )charset utf8;

        alter table my_uniq3 add unique key(username);

    查看唯一键
        表属性可以通过查看表结构来实现
        查看表结构实现查看唯一键

        desc my_uniq3

        可以为空default为null

        在不为空的情况下，允许插入互不相同的数据

    删除唯一键

        一个表中允许存在多个唯一键
        删除需要制定唯一键
        删除的基本语法:
            alter table 表名 drop index 唯一键的名字;
        
            index:索引，唯一键是索引的一种，索引是用来提升查询效率的

        example:
            删除一个索引
                alter table my_uniq2 drop index username;

    修改唯一键：没有这个功能_只能先删除，后增加

    复合唯一键:
        可以使用多个字段来保证唯一性
        一般主键都是单一字段，不带有业务意义
        而其他需要唯一性的内容，都是由唯一键来处理
        
