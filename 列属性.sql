--列属性:
    一共有六个属性
    null，默认值，列描述，主键，唯一键，自动增长

    Null属性:
        NULL_字段为空
        如果对应的值为yes，表示该字段可以为null
        在设计表的时候，尽量不要为空

    默认值：
    当字段被设计的时候，如果允许默认情况下，用户不进行
    数据的插入，那么可以用实现准备好的数据来进行填充
    Default:
        设计
    创建表:
        create table my_default(
            name varchar(10) NOT NULL,
            age default 18 --如果当前字段在数据插入时没有提供，就自动填充18
        )charset utf8;

    插入数据：

        insert into my_default (name) values ('Tom');

        然后会自动插入数据 age 18

        显示的告知字段使用默认值:
            在进行数段插入的时候，可以直接指定default,会自动填入默认值
            insert into my_default values ('jack',default);
    
-- 列描述:
    comment专门给开发人员进行维护的一个注释说明
    基本语法
        comment '字段描述';

    创建表，增加字段描述
    create table my_comment(
        name varchar(10) not null comment '当前是用户名,用户名不能为空',
        pass varchar(50) not null comment '当前是用户密码，且不能为空'

    )charset utf8;


    查看comment
    必须通过查看表创建语句才可以查看
    show create table my_comment;

    给别人给自己提供方便，给以后查看表的人知晓表设计者的用意。

