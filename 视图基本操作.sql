视图的基本操作:
    视图本质是sql指令，(select语句)
    基本语法：
        create view 视图名字 as select指令;
        只要是select语句都可以放上来
    
    创建视图:
        create view student_class_v as
        select * from my_student as s left join
        my_class as c
        on
        s.class_id = c.class_id;
        --------error
        视图也是一张表，一张表的字段不可以同名
        要是同名了，就要取别名
        或者其他方式：

        create view student_class_v as
        select s.*,c.name from my_student as s left join
        my_class as c
        on
        s.class_id = c.class_id;

        s.* : 取这个表的所有字段
        c.name : c表中的name字段

    查看视图结构：视图本身是虚拟表，所以关于表的一些操作
    都适用于视图

    desc 视图名字

    使用视图
    视图是虚拟表
    视图本身是没有数据的
    是临时执行select语句得到的对应结果
    
    视图主要用于给用户进行查询操作

    基本语法：  
        select 字段列表 from 视图名字;

    修改视图：
        本质是修改视图对应的查询语句
        基本语法：
            alter view 视图名字 as 新的select语句;

            alter viex student_class_v as 
            select * from my_student as s left join
            my_class as c
            using(class_id);
    删除视图
    drop view 视图名字
    drop view student_class_v;