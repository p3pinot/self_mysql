-- 主键
    主要的键，primary key,
    在一张表中，有且仅有一个字段，里面的值具有唯一性

    几乎每一张表都有主键

    -- 创建主键
        随表创建主键:
            两种增加主键的方式
            1、在需要当做主键的字段之后增加primary key
            2、在所有字段之后增加primary key primary key('字段信息')

        --在字段后增加主键属性

        create table my_pri1(
            username varchar(10) primary key
        )charset utf8;

        create table my_pri2(
            username varchar(10),
            primary key(username)
        )charset utf8;

        主键是不能为空的！！！

        表后添加主键:
        alter table 表名 add primary key(字段);

    --查看主键
    查看表结构

    --删除主键
        alter table 表名 drop primary key;

    --复合主键
        example：
            学生选修课表，一个学生可以选修多个选修课
            一个选修课可以由多个学生来选
            但是一个学生在一个选修课中只有一个成绩
            使用复合主键，让这个学生，在一个选修课下只有一个成绩

        create table score(
            student char(10),
            course char(10),
            score tinyint not null,
            primary key(student,course)
        )charset utf8;
    
        一般用于中间表的创建

    --主键约束
        主键一旦增加，那么对应的字段有数据要求
        1.当前字段对应数据不能为空
        2.当前字段对应的数据，不能有任何的重复

        测试pass
    
        同时主键的情况下，只要有一个数据非重复，就不会被判断为重复
        
    --主键分类
        主键分类采用的是主键所对应字段的业务意义来进行分类
        1.业务主键:主键所在的字段,具有业务意义（学生ID，课程ID）
        2.逻辑主键：自然增长的整型（应用广泛增加逻辑主键，保证数据的唯一性）
        