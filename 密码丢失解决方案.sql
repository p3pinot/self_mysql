密码丢失的解决方案：
    如果忘记了root用户密码，就需要去找回，或者
    重置root用户密码

    1.停止服务
        net stop mysql
    2.重新驱动服务
        mysqld.exe --skip-grant-tables
        目的：跳过权限并且启动mysql服务
    3.当前启动的服务器没有权限概念，这个情况下是
    是非常危险的，任何客户端，不需要任何用户信息
    都可以直接登录，并且是最高权限，
    mysql ------>直接进入sql数据库
    4.修改root用户密码
    指定用户名@主机地址
    update mysql.user set password = password('新密码') where user = 'root' and host = 'localhost';
    5.关闭服务器
        可以直接从任务管理器里面进行关闭
    6.重启服务
        
