--显示所有数据库
show databases;

--查看以my开头的数据库
show databases like 'my%';

--查看数据库创建语句
show create database 数据库名称

--选择数据库
use 数据库名称;
database changes
--进到了对应数据库

--修改数据库
    --修改数据库的字符集(库选项)
    --不能修改数据库名字(安全性提高)
    alter database 数据库名字 charset 字符集（例如gbk）;

--删除数据库
    drop database 数据库名字;
    
    --删库要做好安全操作，确保数据没有问题
    

