-- 自动增长
    auto_increment
        在给定某个字段该属性之后，在该列的数据在没有提供
        确定的数据的时候，系统就会根据已经存在的数据进行自动增加后。
        填充数据


        通常自动增长，用于逻辑主键

        1.原理：系统中有维护一组数据
            用来保存当前使用了自动增长的字段，记住当前数据值
            给个指定的步长
        2.在用户进行数据插入时，系统在原始值上再加上步长
            变成新的数据
        3.自动增长的出发，给定属性的字段没有提供值
        4.自动增长只适用于数值。
        

    使用自动增长
        基本语法:
        在字段之后增加一个属性:
            auto_increment
        
        example:
            create table my_auto(
                id int primary key auto_increment,
                name varchar(10) not null comment '用户名称',
                pass varchar(50) not null comment '用户密码'
            )charset utf8;
        mysql> desc my_auto;
        +-------+-------------+------+-----+---------+----------------+
        | Field | Type        | Null | Key | Default | Extra          |
        +-------+-------------+------+-----+---------+----------------+
        | id    | int         | NO   | PRI | NULL    | auto_increment |
        | name  | varchar(10) | NO   |     | NULL    |                |
        | pass  | varchar(50) | NO   |     | NULL    |                |
        +-------+-------------+------+-----+---------+----------------+
        3 rows in set (0.00 sec)

        插入数据(触发自动生长，不能给定具体值)
            insert into my_auto values (null,'tom','123456');

        mysql> select * from my_auto;
        +----+------+--------+
        | id | name | pass   |
        +----+------+--------+
        |  1 | tom  | 123456 |
        +----+------+--------+
        1 row in set (0.00 sec)
        id中的1就是通过自增长而产生的

    自动增长高级操作:
        修改自动增长:
            1.查看自增长,自增长一旦触发使用，会自动在表选项中，增加一个选项
            2.一张表只能有一个自增长，一般是id
            3.表选项可以通过修改表结构来实现
            alter table 表名 auto_increment = 值；

            example:
                alter table my_auto auto_increment = 10;
    
            删除自增长:
                删除自增长，在字段属性之后不再保留auto_increment,
                用户在修改自增长所在字段的时候，如果没有看到auto_increment字段
                系统就会自动删除该字段的自增长

            修改属性使用modify方法
                example：
                    alter table my_auto modify id int;
                ！！！不要增加primary Key


            细节问题:
                1、一张表只能有一个自增长，自增长会上升到表选项中，所以一个表里面只有一个自增长
                2、如果数据中插入没有触发自增长，那么自增长是不会表现出来的
                