函数
    函数分为内置函数和用户自定义函数
    
    不管是内置函数还是用户自定义函数
    select 函数名 (参数列表);



    字符串函数：
    char_length()：判断字符串的字符数
    length():判断字符串的字节数
    concat():连接字符串
    instr():判断字符在目标字符串中是否存在，存在返回其所在的位置，不存在返回0
    Lcase():全部小写
    Left():从左侧指定位置开始截取字符串
    ltrlm():消除左边对应的空格
    mid():从中间指定位置开始截取，如果不指定截取长度
            截取到最后

    ！！在mysql中第一个是从1开始，而不是和其他函数一样从0开始

    时间函数：
        now():返回当前时间，日期，时间
        curdate():返回当前日期
        curtime(): 返回当前时间
        datediff(): 判断两个日期的天数相差多少
        date_add(): 进行时间的增加
            (日期,interval 时间数字 type)
                type: second/hour/min/day ...可查看函数解释
        unix_timestamp(): 获取当前时间戳
        from_unixtime():将制定时间戳转换成对应的日期时间格式

    数学函数：
        abs():取绝对值
        ceiling():向上取整
        Floor():向下取整
        pow():求指数，x的v次方
        rand():获取0-1之间的随机数
        round():四舍五入函数

    其他函数：
        md5():对数据进行md5加密 _ 统一加密算法
        version()：获取版本号
        databse():显示当前数据库
        UUID():生成一个唯一标识符，自增长：自增长是单表唯一
                UUID 是整库（数据唯一同时空间唯一）

    自定义函数：
        用户自定义的函数
        函数：
            实现某种功能的多条语句块（由多条语句组成）

            1.函数内部的每条语句，都是独立的个体，都要符合语句的定义规范
            需要语句结束符分号;

            2.函数是一个整体，而且是在调用的时候才会被执行，那么当
            设计函数的时候，意味着整体不能被中断

            3.mysql一旦见到语句结束符号，就会自动开始执行

            解决方案
            在定义函数之前，尝试修改临时的语句结束符.
            基本语法：  delimiter
            修改临时语句结束符
            delimiter 新符号(可以使用系统不识别的符号$$)
            中间使用正常的sql指令，使用分号结束（系统不会执行）
            直到使用新符号结束
            然后修改回语句结束符delimiter ;

            创建函数：

            需要查询函数创建变量是否打开
            mysql> show variables like '%func%';
            +---------------------------------+-------+
            | Variable_name                   | Value |
            +---------------------------------+-------+
            | log_bin_trust_function_creators | OFF   |
            +---------------------------------+-------+
            1 row in set, 1 warning (0.01 sec)

            mysql> set global log_bin_trust_function_creators=1;
            Query OK, 0 rows affected (0.00 sec)

            mysql> show variables like '%func%';
            +---------------------------------+-------+
            | Variable_name                   | Value |
            +---------------------------------+-------+
            | log_bin_trust_function_creators | ON    |
            +---------------------------------+-------+
            1 row in set, 1 warning (0.00 sec)
                
                基本语法：
                function 函数名,函数名,参数(形参和实参[可选])，确认函数返回值类型，函数体，返回值

                函数定义基本语法：
                    create function 函数名(形参) returns 返回值类型
                    begin
                        //函数体
                        return 返回值数据;
                        (数据必须和返回值类型一致，否则错误)
                    end
                    语句结束符

             创建没有参数的函数
                创建自定义函数
                    --修改语句结束符
                    mysql> delimiter $$
                    mysql> create function my_fun()
                        -> returns int
                        --一定要returns才可以
                        -> NO SQL
                        -> begin
                        ->  return 10;
                        -> end $$
                    Query OK, 0 rows affected (0.01 sec)

                
