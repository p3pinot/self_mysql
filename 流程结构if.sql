流程结构：
    代码的执行顺序
if分支结构：
    基本语法：
        if在myysql中有两种基本用法：
            1.用在select条件中，当做一种条件来进行判断
            2.用在复杂的语句块中，函数，存储过程，触发器
            1.在select中使用
                if(条件，结果为真,为假结果)
                example:
                    select * from my_student;
                    select *,if(stu_age>20,'符合条件','不符合条件') as judge(新字段名称) from my_student where stu_age > 20;
                    select *,if(stu_age>20,'符合条件','不符合条件') as judge from my_student;
            2.在复杂语句块中进行使用：
                if 条件表达式 then
                    满足条件要执行的语句；
                End if;
    复合语法：
        代码的判断存在两面性，两面对应都有对应的代码需要执行。
        基本语法:
            if 条件表达式 then  
                满足条件要执行的语句；
            else
                不满足条件要执行的语句：
            end if;
    
        如果还有其他的分支，使用if
        if 条件表达式 then
            满足条件要执行的语句；
        else 
            不满足条件要执行的语句：
            if 条件表达式 then
                满足条件要执行的语句；
            end if
        end if
