联合查询：
    将多个查询的结果合并到一起
    纵向合并，字段数不变，多个查询的记录合并
    union union all
    应用场景：
        将一个表中的不同的结果，需要对应多条查询语句来实现，合并到一起展示数据

        男生身高升序排列，女生身高降序排列
        (select * from my_student where gender = '男' order by stu_height asc)
        union （默认选项是distinct）
        (select * from my_student where gender = '女' order by stu_height desc);
        数据量大的情况下，会对表进行分表操作，操作需要对每一张表进行部分数据统计
        使用联合查询来将数据存放到一起显示。
        QQ1表获取在线数据
        QQ2表获取在线数据
        ----将所有的结果显示出来

    基本语法：  
        select 语句
        union [union选项]
        select 语句;

            union理论上只要保证字段数一样,不需要每次拿到的数据对应的字段类型一致
        union 选项：
        distinct,去重,去掉完全相同的数据
        all , 保留所有的数据

        select (字段列表) 
        union distinct 
        select (字段列表);

        字段列表长度要一样，不用关顾数据类型
    order by的使用
    在联合查询中，如果要使用order by，那么对应的select语句必须需要使用括号括起来

    example:
    这个语句里面有order by
    (select * from my_student where gender = '男' order by stu_height asc)
    union （默认选项是distinct）
    (select * from my_student where gender = '女' order by stu_height desc);
    而且如果想要order by生效，就需要用limit
    (select * from my_student where gender = '男' order by stu_height asc limit 10)
    union （默认选项是distinct）
    (select * from my_student where gender = '女' order by stu_height desc limit 10);
    limit可以随便填,一般大于表的大小
