运算符
    算数运算符:
    在select字段中运算
    -比较运算符:
    <,>=,<=,=,<>
    在条件中进行限定结果
    在mysql中没有对应的==比较符号
    相等比较<=>、=

    特殊运用:
    select 没有规定必须使用数据表
    select 'a' <=> 1,0.02<=>0;
    数据会自动转换成同类型后进行比较

    -查找身高区间和年龄区间
    between是个闭区间
    select * from my_student where stu_age between 20 and 30;
    between 在条件中,条件一必须小于条件二


    -逻辑运算符
    and or not

    select * from my_student where stu_age>=20 and stu_age<=30;

    查找身高大于170的或者年龄大于20岁的学生
    select * from my_student where stu_height>170 or stu_age>20;

    In运算符:
    in 在什么里面，是用来替代=时候，当结果不是一个值而是一个集的时候
    基本语法in(结果1，结果2，结果3),只要当前条件在结果集中就返回true

    知道学生学号信息，得所有信息：
        select * from my_student where stu_id in ('stu001','stu0002');
    
    is运算符
        is专门用来判断字段是否为null的运算符
        is null 
        is not null
    like运算符
        进行模糊匹配的
            两种占位符
            _,匹配对应的单个字符
            %,匹配多个字符
        select * from my_student where stu_name like '小%';


    


