事务基本原理：
    事务处理：
        事务在mysql中是自动提交的，可以使用手动事务

    自动事务：
        自动事务：autocommit，当客户端发送一条sql指令
                增删改查操作
                服务器在执行之后，不用等待用户反馈结果
                会自动将结果同步到数据表
                不需要我们去处理

                证明：
                    利用两个客户端，一个客户端执行sql指令
                    另外一个客户端，查看执行结果

                自动事务：系统做了额外的步骤来帮助用户操作
                系统是通过变量来控制的，autocommit变量

                show variables like 'autocommit';

                ON-->开启了自动事务

                关闭自动事务：
                    关闭之后，系统就不再帮助用户提交结果了

                    set autocommit = off;

                    另外一个客户端看不到结果
                    一旦自动事务关闭，需要用户提供是否同步的指令
                    commit:提交，同步到数据表,事物也会被清空
                    rollback：回滚，清空之前的操作
                手动事务：
                    手动事务：不管是开始还是过程还是结束
                    都需要数据库管理员手动发送对应的事务操作指令

                    基本命令：
                        开启事务：
                        start transaction; 
                        //从这条语句开始，所有操作都不会被写到数据表
                        事务处理
                        多个写指令
                        事务提交：
                            commit/rollback,到这个时候所有的事务才算结束
                        
                        手动事务：
                            开启事务：
                            start transaction;
                            执行事务：
                            插入新数据（事务操作1）
                            insert into my_class value ('数据1','数据2');
                            修改对应表数据（事务操作2）
                            update my_student set my_class_id = 6 where stu_id = 'stu0007';
                            --事务操作结束
                            提交事务
                            commit 数据会写到数据表
                            rollback 所有数据失效，并且清空

                        回滚点：
                            savepoint 当有一系列的事务操作时，
                            如果其中有些步骤成功了，没有必要重新来过
                            可以在某个成功点，设置一个记号，称之为回滚点

                            如果中间又失败，就可以回滚到回滚点处
                            相当于一个橡皮擦

                            增加回滚点
                            savepoint 回滚点名字; //字母和数字
                            回到回滚点
                            rollback to 回滚点名字；
                            记号之后的所有操作，没有了

                            事务处理中,如果有多个步骤，可以设置多个回滚点，
                            但是如果回到了前面的回滚点，后面的回滚点就失效了

                            example:
                                start transaction;
                                insert into my_calss values('2','2班');
                                --有可能失败，增加一个回滚点
                                savepoint spv1;
                                update my_class set class_id = 2 where class_id = 'stu0005';
                                --如果出错
                                rollback to spv1;
                                --回滚到回滚点
                    事务特性：
                        原子性
                        一致性
                        隔离性
                        持久性

                                                        



                        
